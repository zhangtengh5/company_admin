import Vue from 'vue';
import Router from 'vue-router';
import store from '../vuex/index'
import axios from '../axios/http'
Vue.use(Router);

let router = new Router({
    routes: [{
            path: '/',
            redirect: '/dashboard'
        },
        {
            path: '/',
            component: resolve => require(['../components/common/Home.vue'], resolve),
            meta: {
                title: ''
            },
            children: [{
                path: '/dashboard',
                    component: resolve => require(['../components/page/Dashboard.vue'], resolve),
                    meta: {
                        title: '系统首页',
                        requiresAuth: true
                    }
                },
                {
                    path: '/product',
                    component: resolve => require(['../components/page/Product.vue'], resolve),
                    meta: {
                        title: '产品管理',
                        requiresAuth: true
                    }
                },
                {
                    path: '/company',
                    component: resolve => require(['../components/page/Company.vue'], resolve),
                    meta: {
                        title: '组织管理',
                        requiresAuth: true
                    }
                },
                {
                    path: '/activity',
                    component: resolve => require(['../components/page/Activity.vue'], resolve),
                    meta: {
                        title: '活动管理',
                        requiresAuth: true
                    }
                },
                {
                    path: '/notice',
                    component: resolve => require(['../components/page/Notice.vue'], resolve),
                    meta: {
                        title: '公告管理',
                        requiresAuth: true
                    }
                },
                {
                    path: '/dolog',
                    component: resolve => require(['../components/page/Dolog.vue'], resolve),
                    meta: {
                        title: '操作日志',
                        requiresAuth: true
                    }
                }
            ]
        },
        {
            path: '/login',
            component: resolve => require(['../components/page/Admin.vue'], resolve)
        },
        {
            path: '*',
            redirect: '/404'
        },
        {
            path: '/404',
            component: resolve => require(['../components/page/404.vue'], resolve)
        }
    ]
})

router.beforeEach((to, from, next) => {
    let data = window.sessionStorage.getItem('ztmjadmin')
    if (data) {
      //登录信息
      store.commit({
        type: 'userInfo',
        data: JSON.parse(data)
      })
    }
    if (to.matched.some(record => record.meta.requiresAuth)) {
        let adminId = store.getters.adminId
        axios.post('/admin/GetMySession.do', {})
        .then((response) => {
            let result_data = response.data;
            if (parseInt(result_data.status) === 1) {
                if (adminId) {
                    next()
                } else {
                    next({
                        path: '/login'
                    })
                }
            } else {
                next({
                    path: '/login'
                })
            }
        })
        .catch((error) => {
            next({
                path: '/login'
            })
        })
    } else {
      next() // 确保一定要调用 next()
    }
})

export default router;
