const user = {
    state: {
      topath: '',
      frompath: '',
      collapse: false
    },
    mutations: {
      path(state, payload) {
        let path = payload.path;
        state.frompath = path.from;
        state.topath = path.to;
      },
      iscollapse(state, payload) {
        state.collapse = !state.collapse
      }
    },
    actions: {

    }
}

export default user