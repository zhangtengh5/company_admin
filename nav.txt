{
  icon: "el-icon-lx-home",
  url: "/dashboard",
  name: "系统首页"
},
{
  icon: "el-icon-lx-cascades",
  url: "/product",
  name: "产品管理"
},
{
  icon: "el-icon-lx-cascades",
  url: "/company",
  name: "公司管理"
},
{
  icon: "el-icon-lx-cascades",
  url: "/activity",
  name: "活动管理"
},
{
  icon: "el-icon-lx-cascades",
  url: "/notice",
  name: "公告管理"
},
{
  icon: "el-icon-lx-cascades",
  url: "/equipment",
  name: "设备管理"
},
{
  icon: "el-icon-lx-cascades",
  url: "/role",
  name: "角色管理"
},
{
  icon: "el-icon-lx-cascades",
  url: "/employee",
  name: "人员管理"
},
{
  icon: "el-icon-lx-cascades",
  url: "/passlog",
  name: "通行日志"
},
{
  icon: "el-icon-lx-cascades",
  url: "/dolog",
  name: "操作日志"
}

let arrStr = "[{"passWayId":6,"passWayName":"东门","passTimeList":["2018-12-04 00:00:00,2018-12-05 00:00:00"]},{"passWayId":7,"passWayName":"西门","passTimeList":["2018-12-04 00:00:00,2018-12-05 00:00:00"]},{"passWayId":8,"passWayName":"南门","passTimeList":["2018-12-04 00:00:00,2018-12-05 00:00:00","2018-12-04 00:00:00,2018-12-05 00:00:00"]}]"